FROM node:10-alpine as compiler

WORKDIR /usr/app

COPY package.json .

RUN yarn

COPY src src

RUN yarn run build

FROM nginx:alpine

COPY --from=compiler /usr/app/dist /usr/share/nginx/html

EXPOSE 80
